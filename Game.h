#ifndef GAME_H
#define GAME_G

#include "Direct3D.h"
#include "InputController.h"
#include "Player.h"
#include "Shader.h"
#include "TileTypes.h"
#include "MeshManager.h"
#include "TextureManager.h"
#include "CameraMoving.h"
#include "Button.h"
#include "CollisionManager.h"
#include "Capsule.h"
#include "Enemy.h"
#include "Bullet.h"

#include "DirectXTK/SpriteBatch.h"
#include "DirectXTK/SpriteFont.h"

#include <vector>

class Game {

private:
	InputController* m_input;
	Direct3D* m_renderer;
	Camera* m_currentCam;
	Shader* m_shader;
	Mesh* m_tileMesh;
	Mesh* m_playerMesh;
	Mesh* m_wallMesh;
	Mesh* m_capsuleMesh;
	Mesh* m_enemyMesh;
	Mesh* m_bulletMesh;
	Button* m_buttonYes;
	Button* m_buttonNo;
	bool m_running = true;
	bool m_gameOver = false;
	MeshManager* m_meshManager = new MeshManager();
	TextureManager* m_textureManager = new TextureManager();
	SpriteBatch* m_spriteBatch;
	SpriteFont* m_spriteFont;
	RECT* m_healthSize;
	RECT* m_screenSize;
	CollisionManager* m_collisionManager;

	Shader* m_unlitVertexColouredShader;
	Shader* m_unlitTexturedShader;

	std::vector<GameObject*> m_tiles;
	std::vector<GameObject*> m_walls;
	std::vector<Capsule*> m_healthCapsules;
	std::vector<Capsule*> m_teleCapsules;
	std::vector<Capsule*> m_capsules;
	std::vector<GameObject*> m_gameObjects;
	std::vector<Enemy*> m_enemies;
	std::vector<Button*> m_buttons;
	std::vector<std::vector<GameObject*>> m_board;
	std::vector<std::vector<Bullet*>> m_clips;
	Player* m_player;

	std::wstring m_score;
	std::wstring m_gameOverTxt;
	std::wstring m_continue;

	Texture* m_healthSprite;
	Texture* m_grayBack;

	bool InitShaders();
	bool LoadMeshes();
	bool LoadTextures();
	void InitGameWorld();
	void LoadFonts();
	void InitUI();
	void DrawUI();
	void DrawGameOverUI();
	void RefreshUI();
	void MakeBoard();
	void GameOver() { m_running = false; }

public:
	Game();
	~Game();

	void Update(float timestep);
	void Render();
	void Shutdown();
	bool getGameOver() { return m_running; }
	void Restart();

	bool Initialise(Direct3D* renderer, InputController* input);

};

#endif