#include "Player.h"
#include "Game.h"
#include "CameraMoving.h"

#include <cstdlib>
#include <algorithm>

Player::Player(InputController* input){

	m_input = input;

	Initialize();

	m_boundingSphere = CBoundingSphere(m_position, m_playerRadius);

}

void Player::Initialize() {

	m_teleportCount = m_startCount;

	setStart(); //Sets the starting point of the player

}

void Player::Update(float timestep) {

	m_shootBuffer += timestep;
	m_currentTimestep = timestep;
	m_lastPosition = m_position;

	Matrix heading = Matrix::CreateRotationY(m_heading);
	Vector3	localForward = Vector3::TransformNormal(worldForward, heading);
	Vector3 localRight = Vector3::TransformNormal(worldRight, heading);
	Vector3 localRightDiag = (localForward + localRight) * m_diagBuffer;
	Vector3 localLeftDiag = (localForward - localRight) * m_diagBuffer;

	if (m_input->GetKeyHold('W')) {

		if (m_input->GetKeyHold('D')) {

			m_position += localRightDiag * (timestep + m_moveSpeed);

		}
		else if (m_input->GetKeyHold('A')) {

			m_position += localLeftDiag * (timestep + m_moveSpeed);

		}
		else if (m_input->GetKeyHold('S')) { ; }
		else m_position += localForward * (timestep + m_moveSpeed);

	}
	else if (m_input->GetKeyHold('S')) {


		if (m_input->GetKeyHold('D')) {

			m_position -= localLeftDiag * (timestep + m_moveSpeed);

		}
		else if (m_input->GetKeyHold('A')) {

			m_position -= localRightDiag * (timestep + m_moveSpeed);

		}
		else m_position -= localForward * (timestep + m_moveSpeed);

	}
	else if (m_input->GetKeyHold('A')) {

		m_position -= localRight * (timestep + m_moveSpeed);

	}
	else if (m_input->GetKeyHold('D')) {

		m_position += localRight * (timestep + m_moveSpeed);

	}

 	if (m_input->GetKeyHold(VK_SPACE)) {

		if (m_shootBuffer - m_lastShootBuffer > m_shootTime) {

			Shoot();
			m_lastShootBuffer = m_shootBuffer; 

		}

	}

	if (m_position.y != m_startPosition.y) m_position.y = m_startPosition.y;
	m_boundingSphere.SetCenter(m_position);

}

bool Player::checkPlayer() {

	if (m_health <= 0.0f) return true; //Is the plyer's health below zero?
	else return false;

}

void Player::heal() {

	if (m_health > m_health - m_healthIncrement) m_health = m_maxHealth; //If the player is above 150 health, make it max health of 200
	else m_health += m_healthIncrement; //Otherwise add 50 health

}

void Player::teleport(Capsule* capsule) {

	bool found = false;

	while (!found && m_teleportCount < 10) { //While a suitable teleport tile hasn't been found

		Capsule* m_capsule = (*m_teleportList)[m_teleportCount];

		if (m_capsule->GetEnabled()) {

			if (capsule == m_capsule) {

				Capsule* tempCapsule = m_capsule;

				for (int i = 0; i < (*m_teleportList).size(); i++) {

					if ((*m_teleportList)[i]->GetEnabled() && (*m_teleportList)[i] != m_capsule) {

						m_capsule = (*m_teleportList)[i];
						(*m_teleportList)[i] = tempCapsule;

					}

				}

			}
			m_capsule->Disable();
			m_position = m_capsule->GetPosition();
			(*m_teleportList)[m_teleportCount] = m_capsule;
			m_teleportCount++;
			found = true;

		}
		else m_teleportCount++;

	} 

}

void Player::setStart() {

	m_position = m_startPosition;

}

bool Player::checkGameOver() {

	if (checkPlayer()) return true; //Checks game over by checking player health. monster kills, and surrounding tiles aren't all disabled or walls
	if (m_killCount == m_maxKillCount) return true;

	return false;

}

void Player::Reset() {

	m_health = m_maxHealth; //Resets all the starting values
	m_killCount = m_startCount;
	Initialize();

}

void Player::Release() {

	if (m_input) {

		m_input = NULL;

	}

}

void Player::Shoot() {

	(*m_bullets)[m_shootCount]->Enable();
	(*m_bullets)[m_shootCount]->SetPosition(m_position + Vector3::TransformNormal(m_bulletOffset, Matrix::CreateRotationY(m_heading)));
	(*m_bullets)[m_shootCount]->SetRotY(m_heading);
	(*m_bullets)[m_shootCount]->SetDamage(MathsHelper::RandomRange(m_damageDown, m_damageUp));

	if (m_shootCount >= (*m_bullets).size() - 1) m_shootCount -= (*m_bullets).size() - 1;
	else m_shootCount++;

}

void Player::WallCollision(GameObject* *wall) {

	if ((*wall)->GetPosition().x < 0) {

		m_position += worldRight * (m_moveSpeed + m_currentTimestep);

	}
	else if ((*wall)->GetPosition().x > 9) {

		m_position -= worldRight * (m_moveSpeed + m_currentTimestep);

	}
	else if ((*wall)->GetPosition().z < 0) {

		m_position += worldForward * (m_moveSpeed + m_currentTimestep);

	}
	else if ((*wall)->GetPosition().z > 9) {

		m_position -= worldForward * (m_moveSpeed + m_currentTimestep);

	}

}

void Player::CapsuleCollision(Capsule* *capsule) {

	if ((*capsule)->GetType() == Type::Heal && m_health != m_maxHealth) {

		heal();
		(*capsule)->Disable();

	}
	else if ((*capsule)->GetType() == Type::Teleport) {

		teleport((*capsule));
		(*capsule)->Disable();

	}

}

void Player::BulletCollision(Bullet* *bullet) {

	(*bullet)->Disable();
	m_health -= (*bullet)->GetDamage();

}