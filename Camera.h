#ifndef CAMERA_H
#define CAMERA_H

#include "Direct3D.h"
#include "DirectXTK/SimpleMath.h"
#include "MathsHelper.h"

using namespace DirectX::SimpleMath;
 
class Camera
{
private:
	Vector3 m_position;
	Vector3 m_lookAtTarget;
	Vector3 m_up;

	float m_aspectRatio;
	float m_fieldOfView;
	float m_nearClip;
	float m_farClip;

	Matrix m_view;
	Matrix m_projection;

	bool m_viewDirty;
	bool m_projectionDirty;

public:
	Camera();
	Camera(Vector3 pos, Vector3 lookAt, Vector3 up, float aspect, float fov, float nearClip, float farClip);
	~Camera();

	void SetPosition(Vector3 pos);
	void SetLookAt(Vector3 lookAt);
	void SetUp(Vector3 up);

	void SetAspectRatio(float aspect);
	void SetFieldOfView(float fov);
	void SetNearClip(float nearClip);
	void SetFarClip(float farClip);

	Matrix GetView() { return m_view; }
	Matrix GetProjection() { return m_projection; }
	Vector3 GetPosition() { return m_position; }
	Vector3 GetLookAt() { return m_lookAtTarget; }

	virtual void Update(float timestep);
};


#endif