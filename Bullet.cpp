#include "Bullet.h"

Bullet::Bullet(Mesh* mesh, Shader* shader, Texture* texture) :
	GameObject(mesh, shader, texture) {
}

void Bullet::Update(float timestep) {

	if (m_enabled) {

 		if (m_timeDelay >= m_timeBuffer) m_damageEnabled = true;
 		else m_timeDelay += timestep;

		Matrix heading = Matrix::CreateRotationY(m_rotationY);
		Vector3	localForward = Vector3::TransformNormal(worldForward, heading);

		m_position += localForward * timestep * m_movespeed;

		m_boundingBox.SetMin(m_position + m_mesh->GetMin());
		m_boundingBox.SetMax(m_position + m_mesh->GetMax());

	}

}

void Bullet::Enable() {

	m_enabled = true;
	m_timeDelay = m_timeDelayStart;

}

void Bullet::Disable() {

	m_enabled = false;
	m_damageEnabled = false;

}