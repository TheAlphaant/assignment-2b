#pragma once
#include "GameObject.h"
#include "Player.h"
#include "Bullet.h"

class Enemy : public GameObject{
protected:
	Vector3 m_target;
	Player* *m_player;
	MonsterType m_type;
	bool m_enabled = true;
	float m_currentTimestep;
	std::vector<Bullet*>* m_bullets;

	const int m_startCount = 0;
	int m_shootCount = 0;

	float m_health;
	float m_healthTop = 325.0f;
	float m_healthBottom = 100.0f;
	float m_moveSpeed;

	float m_health1 = 300.0f;
	float m_health2 = 250.0f;
	float m_health3 = 200.0f;
	float m_health4 = 150.0f;
	float m_health5 = 100.0f;

	float m_speed1 = 0.20f;
	float m_speed2 = 0.25f;
	float m_speed3 = 0.35f;
	float m_speed4 = 0.5f;
	float m_speed5 = 0.65f;

	float m_damageDown = 0.15f;
	float m_damageUp = 0.25f;

	float m_shootBuffer;
	float m_lastShootBuffer;
	float m_shootTime;
	float m_shootTimeTop = 0.8f;
	float m_shootTimeBottom = 1.5f;
	Vector3 m_bulletOffset = Vector3(-0.0665f, 0.6f, 0.0685f);

	float m_damageTop = 50.0f;
	float m_damageBottom = 10.0f;

	int m_worldTop = 9;

	float m_startScale = 0.5f;
	float m_moveBuffer = 0.5f;

	CBoundingSphere m_playerEnemyBuffer;
	float m_playerEnemyBufferSize = 2.0f;

	Vector3 RandomSpot() { return Vector3(MathsHelper::RandomRange(0, 9), 0, MathsHelper::RandomRange(0, 9)); }

	void Initialize();
	void Shoot();
	
public:
	Enemy::Enemy(Mesh* mesh, Shader* shader, Texture* texture, Player* *player, MonsterType tpye);

	virtual void Update(float timestep);
	virtual void Render(Direct3D* renderer, Camera* cam);

	void Check() { if (m_health <= 0.0f) m_enabled = false; }
	void WallCollision(GameObject* *wall);
	void BulletCollision(Bullet* *bullet);

	void SetClip(std::vector<Bullet*>* clip) { m_bullets = clip; }

	void Reset() { Initialize(); }

	bool GetEnabled() { return m_enabled; }

	bool CheckDeath();

};