#pragma once

#include "GameObject.h"

class Bullet : public GameObject {

protected:
	const float m_movespeed = 3.0f;
	const int m_startCount = 0;
	float m_timeDelay;
	float m_timeDelayStart = 0.0f;
	float m_timeBuffer = 0.2f;
	bool m_enabled = false;
	bool m_damageEnabled = false;

	float m_damage;

public:
	Bullet(Mesh* mesh, Shader* shader, Texture* texture);
	virtual void Update(float timestep);
	virtual void Render(Direct3D* renderer, Camera* cam) { if (m_enabled) GameObject::Render(renderer, cam); }

	void Enable();
	void Disable();

	float GetDamage() { return m_damage; }
	void SetDamage(float damage) { m_damage = damage; }

	bool GetEnabled() { return m_enabled; }
	bool GetDamageEnabled() { return m_damageEnabled; }

};