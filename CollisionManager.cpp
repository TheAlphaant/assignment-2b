#include "CollisionManager.h"

CollisionManager::CollisionManager(Player* player, std::vector<Enemy*>* enemies, std::vector<GameObject*>* walls, std::vector<GameObject*>* tiles, std::vector<Capsule*>* capsules, std::vector<std::vector<Bullet*>>* bullets)
{
	m_player = player;
	m_enemies = enemies;
	m_walls = walls;
	m_tiles = tiles;
	m_capsules = capsules;
	m_bullets = bullets;

	// Clear our arrays to 0 (NULL)
	memset(m_currentCollisions, m_startCount, sizeof(m_currentCollisions));
	memset(m_previousCollisions, m_startCount, sizeof(m_previousCollisions));

	m_nextCurrentCollisionSlot = m_startCount;
}

void CollisionManager::CheckCollisions()
{
	// Check kart to item box collisions
	PlayerToWall();
	PlayerToCapsule();
	PlayerToEnemy();
	EnemyToWall();
	EnemyToBullet();
	BulletToWall();
	PlayerToBullet();

	// Move all current collisions into previous
	memcpy(m_previousCollisions, m_currentCollisions, sizeof(m_currentCollisions));

	// Clear out current collisions
	memset(m_currentCollisions, m_startCount, sizeof(m_currentCollisions));

	// Now current collisions is empty, we'll start adding from the start again
	m_nextCurrentCollisionSlot = m_startCount;

}

bool CollisionManager::ArrayContainsCollision(GameObject* arrayToSearch[], GameObject* first, GameObject* second)
{
	// See if these two GameObjects appear one after the other in specified collisions array
	// Stop one before length so we don't overrun as we'll be checking two elements per iteration
	for (int i = m_startCount; i < MAX_ALLOWED_COLLISIONS - 1; i += 2)
	{
		if ((arrayToSearch[i] == first && arrayToSearch[i + 1] == second) ||
			arrayToSearch[i] == second && arrayToSearch[i + 1] == first)
		{
			// Found them!
			return true;
		}
	}

	// These objects were not colliding last frame
	return false;
}

void CollisionManager::AddCollision(GameObject* first, GameObject* second)
{
	// Add the two colliding objects to the current collisions array
	// We keep track of the next free slot so no searching is required
	m_currentCollisions[m_nextCurrentCollisionSlot] = first;
	m_currentCollisions[m_nextCurrentCollisionSlot + 1] = second;

	m_nextCurrentCollisionSlot++;
	m_nextCurrentCollisionSlot++;
}

void CollisionManager::PlayerToWall()
{
	// We'll check each kart against every item box
	// Note this is not overly efficient, both in readability and runtime performance

	Player* player = m_player;

	for (unsigned int j = m_startCount; j < m_walls->size(); j++)
	{
		// Don't need to store pointer to these objects again but favouring clarity
		// Can't index into these directly as they're a pointer to a vector. We need to dereference them first
		GameObject* wall = (*m_walls)[j];

		CBoundingSphere playerBounds = player->GetBounds();
		CBoundingBox wallBounds = wall->GetBounds();

		// Are they colliding this frame?
		bool isColliding = CheckCollision(playerBounds, wallBounds);

		// Were they colliding last frame?
		bool wasColliding = ArrayContainsCollision(m_previousCollisions, player, wall);

		if (isColliding)
		{
			// Register the collision
			AddCollision(player, wall);

			if (wasColliding)
			{
				// We are colliding this frame and we were also colliding last frame - that's a collision stay
				// Tell the item box a kart has collided with it (we could pass it the actual kart too if we like)
				player->WallCollision(&wall);
			}
		}
	}
}

void CollisionManager::PlayerToCapsule()
{
	// We'll check each kart against every item box
	// Note this is not overly efficient, both in readability and runtime performance

	Player* player = m_player;

	for (unsigned int j = m_startCount; j < m_capsules->size(); j++)
	{
		// Don't need to store pointer to these objects again but favouring clarity
		// Can't index into these directly as they're a pointer to a vector. We need to dereference them first
		Capsule* capsule = (*m_capsules)[j];

		if (capsule->GetEnabled()) {

			CBoundingSphere playerBounds = player->GetBounds();
			CBoundingBox capsuleBounds = capsule->GetBounds();

			// Are they colliding this frame?
			bool isColliding = CheckCollision(playerBounds, capsuleBounds);

			// Were they colliding last frame?
			bool wasColliding = ArrayContainsCollision(m_previousCollisions, player, capsule);

			if (isColliding)
			{
				// Register the collision
				AddCollision(player, capsule);

				if (wasColliding)
				{
					// We are colliding this frame and we were also colliding last frame - that's a collision stay
					// Tell the item box a kart has collided with it (we could pass it the actual kart too if we like)
					player->CapsuleCollision(&capsule);
					return;
				}
			}
		}
	}
}

void CollisionManager::PlayerToEnemy() {

	Player* player = m_player;

	for (unsigned int j = m_startCount; j < m_enemies->size(); j++)
	{
		// Don't need to store pointer to these objects again but favouring clarity
		// Can't index into these directly as they're a pointer to a vector. We need to dereference them first
		Enemy* enemy = (*m_enemies)[j];

		if (enemy->GetEnabled()) {

			CBoundingSphere playerBounds = player->GetBounds();
			CBoundingBox enemyBounds = enemy->GetBounds();

			// Are they colliding this frame?
			bool isColliding = CheckCollision(playerBounds, enemyBounds);

			// Were they colliding last frame?
			bool wasColliding = ArrayContainsCollision(m_previousCollisions, player, enemy);

			if (isColliding)
			{
				// Register the collision
				AddCollision(player, enemy);
				player->EnemyCollision();
			}
		}
	}
}

void CollisionManager::EnemyToWall() {

	for (unsigned int i = m_startCount; i < m_walls->size(); i++) {

		GameObject* wall = (*m_walls)[i];

		for (unsigned int j = m_startCount; j < m_enemies->size(); j++)
		{
			// Don't need to store pointer to these objects again but favouring clarity
			// Can't index into these directly as they're a pointer to a vector. We need to dereference them first
			Enemy* enemy = (*m_enemies)[j];

			if (enemy->GetEnabled()) {

				CBoundingBox wallBounds = wall->GetBounds();
				CBoundingBox enemyBounds = enemy->GetBounds();

				// Are they colliding this frame?
				bool isColliding = CheckCollision(enemyBounds, wallBounds);

				// Were they colliding last frame?
				bool wasColliding = ArrayContainsCollision(m_previousCollisions, wall, enemy);

				if (isColliding)
				{
					// Register the collision
					AddCollision(wall, enemy);

					if (wasColliding) {

						enemy->WallCollision(&wall);

					}
				}
			}
		}
	}
}

void CollisionManager::BulletToWall() {

	for (unsigned int i = m_startCount; i < m_walls->size(); i++) {

		GameObject* wall = (*m_walls)[i];

		for (unsigned int j = m_startCount; j < m_bullets->size(); j++) {

			for (int k = m_startCount; k < 10; k++) {

				Bullet* bullet = (*m_bullets)[j][k];

				if (bullet->GetEnabled()) {

					CBoundingBox wallBounds = wall->GetBounds();
					CBoundingBox bulletBounds = bullet->GetBounds();

					bool isColliding = CheckCollision(wallBounds, bulletBounds);

					bool wasColliding = ArrayContainsCollision(m_previousCollisions, wall, bullet);

					if (isColliding) {

						AddCollision(wall, bullet);

						if (wasColliding) {

							bullet->Disable();

						}

					}

				}

			}

		}

	}

}

void CollisionManager::EnemyToBullet() {

	for (unsigned int i = m_startCount; i < m_enemies->size(); i++) {

		Enemy* enemy = (*m_enemies)[i];

		for (unsigned int j = m_startCount; j < m_bullets->size(); j++) {

			for (int k = m_startCount; k < 10; k++) {

				Bullet* bullet = (*m_bullets)[j][k];

				if (bullet->GetEnabled() && bullet->GetDamageEnabled() && enemy->GetEnabled()) {

					CBoundingBox enemyBounds = enemy->GetBounds();
					CBoundingBox bulletBounds = bullet->GetBounds();

					bool isColliding = CheckCollision(enemyBounds, bulletBounds);

					bool wasColliding = ArrayContainsCollision(m_previousCollisions, enemy, bullet);

					if (isColliding) {

						AddCollision(enemy, bullet);

						if (wasColliding) {

							enemy->BulletCollision(&bullet);

						}

					}

				}

			}

		}

	}

}

void CollisionManager::PlayerToBullet() {

	Player* player = m_player;

	for (unsigned int j = m_startCount; j < m_bullets->size(); j++) {

		for (int k = m_startCount; k < 10; k++) {

			Bullet* bullet = (*m_bullets)[j][k];

			if (bullet->GetEnabled() && bullet->GetDamageEnabled()) {

				CBoundingSphere playerBounds = player->GetBounds();
				CBoundingBox bulletBounds = bullet->GetBounds();

				bool isColliding = CheckCollision(playerBounds, bulletBounds);

				bool wasColliding = ArrayContainsCollision(m_previousCollisions, player, bullet);

				if (isColliding) {

					AddCollision(player, bullet);

					if (wasColliding) {

						player->BulletCollision(&bullet);

					}

				}

			}

		}

	}

}
