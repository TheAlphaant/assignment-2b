#include "Game.h"
#include "TexturedShader.h"

#include "DirectXTK/CommonStates.h"

#include <cstdlib>
#include <algorithm>
#include <string>
#include <sstream>

Game::Game() {

	m_renderer = NULL;
	m_currentCam = NULL;

}

Game::~Game() {
}

bool Game::Initialise(Direct3D* renderer, InputController* input) {

	m_renderer = renderer;
	m_input = input;

	if (!InitShaders())
		return false;

	if (!LoadMeshes())
		return false;

	if (!LoadTextures())
		return false;

	LoadFonts();
	InitGameWorld();
	InitUI();

	m_currentCam = new CameraMoving(m_player, m_input);
	ShowCursor(false);

	m_collisionManager = new CollisionManager(m_player, &m_enemies, &m_walls, &m_tiles, &m_capsules, &m_clips);

	return true;

}

bool Game::InitShaders()
{
	m_unlitTexturedShader = new TexturedShader();
	if (!m_unlitTexturedShader->Initialise(m_renderer->GetDevice(), L"Assets/Shaders/VertexShader.vs", L"Assets/Shaders/TexturedPixelShader.ps"))
		return false;

	return true;
}

bool Game::LoadMeshes(){


	if (!m_meshManager->Load(m_renderer, "Assets/Meshes/player_capsule.obj")) //Loads the objects
		return false;

	if (!m_meshManager->Load(m_renderer, "Assets/Meshes/floor_tile.obj"))
		return false;

	if (!m_meshManager->Load(m_renderer, "Assets/Meshes/wall_tile.obj"))
		return false;

	if (!m_meshManager->Load(m_renderer, "Assets/Meshes/enemy.obj"))
		return false;

	if (!m_meshManager->Load(m_renderer, "Assets/Meshes/bullet.obj"))
		return false;

	m_tileMesh = m_meshManager->GetMesh("Assets/Meshes/floor_tile.obj");
	m_capsuleMesh = m_meshManager->GetMesh("Assets/Meshes/player_capsule.obj");
	m_wallMesh = m_meshManager->GetMesh("Assets/Meshes/wall_tile.obj");
	m_enemyMesh = m_meshManager->GetMesh("Assets/Meshes/enemy.obj");
	m_bulletMesh = m_meshManager->GetMesh("Assets/Meshes/bullet.obj");

	return true;
}

bool Game::LoadTextures()
{
	if (!m_textureManager->Load(m_renderer, "Assets/Textures/tile_white.png"))
		return false;
	if (!m_textureManager->Load(m_renderer, "Assets/Textures/gray.png"))
		return false;
	if (!m_textureManager->Load(m_renderer, "Assets/Textures/green.png"))
		return false;
	if (!m_textureManager->Load(m_renderer, "Assets/Textures/red.png"))
		return false;
	if (!m_textureManager->Load(m_renderer, "Assets/Textures/blue.png"))
		return false;
	if (!m_textureManager->Load(m_renderer, "Assets/Textures/sprite_healthBar.png"))
		return false;
	if (!m_textureManager->Load(m_renderer, "Assets/Textures/button.png"))
		return false;
	if (!m_textureManager->Load(m_renderer, "Assets/Textures/bullet.png"))
		return false;

	return true;
}

void Game::LoadFonts()
{
	// There's a few different size fonts in there, you know
	m_spriteFont = new SpriteFont(m_renderer->GetDevice(), L"Assets/Fonts/Arial-18pt.spritefont");
}

void Game::InitUI() {

	m_spriteBatch = new SpriteBatch(m_renderer->GetDeviceContext());

	m_healthSprite = m_textureManager->GetTexture("Assets/Textures/sprite_healthBar.png");

	m_healthSize = new RECT();
	m_screenSize = new RECT();

	m_screenSize->top = 0;
	m_screenSize->bottom = 720;
	m_screenSize->left = 0;
	m_screenSize->right = 1280;

	m_grayBack = m_textureManager->GetTexture("Assets/Textures/gray.png");

	m_buttons.push_back(m_buttonYes = new Button(128, 64, m_textureManager->GetTexture("Assets/Textures/button.png"), L"Yes", Vector2(546, 550), m_spriteBatch, m_spriteFont, m_input, [this]
	{
		Restart();
	}));
	m_buttons.push_back(m_buttonNo = new Button(128, 64, m_textureManager->GetTexture("Assets/Textures/button.png"), L"No", Vector2(706, 550), m_spriteBatch, m_spriteFont, m_input, [this]
	{
		GameOver();
	}));

	std::wstringstream ss;
	ss << "GAME OVER!";
	m_gameOverTxt = ss.str();

	std::wstringstream ss2;
	ss2 << "Continue?";
	m_continue = ss2.str();

	RefreshUI();

}

void Game::RefreshUI() {

	std::wstringstream ss;
	ss << "Score: " << m_player->GetKillCount(); //Updates the player's score
	m_score = ss.str();

	m_healthSize->top = 0; //Sets the size of the health bar to be representative of the player's current health
	m_healthSize->bottom = 32;
	m_healthSize->left = 440;
	m_healthSize->right = m_healthSize->left + m_player->GetHealth() * 2;

}

void Game::InitGameWorld()
{
	for (int i = 0; i < 100; i++) {

		m_tiles.push_back(new GameObject(m_tileMesh,
			m_unlitTexturedShader,
			m_textureManager->GetTexture("Assets/Textures/tile_white.png")));

		m_gameObjects.push_back(m_tiles[i]);

	}
	for (int i = 0; i < 10; i++) { //Makes all the tiles and tile objects

		m_healthCapsules.push_back(new Capsule(m_capsuleMesh,
			m_unlitTexturedShader,
			m_textureManager->GetTexture("Assets/Textures/green.png"),
			Type::Heal));

		m_capsules.push_back(m_healthCapsules[i]);
		m_gameObjects.push_back(m_healthCapsules[i]);

	}
	for (int i = 0; i < 10; i++) {

		m_teleCapsules.push_back(new Capsule(m_capsuleMesh,
			m_unlitTexturedShader,
			m_textureManager->GetTexture("Assets/Textures/blue.png"),
			Type::Teleport));

		m_capsules.push_back(m_teleCapsules[i]);
		m_gameObjects.push_back(m_teleCapsules[i]);

	}
	m_enemies.push_back(new Enemy(m_enemyMesh,
		m_unlitTexturedShader,
		m_textureManager->GetTexture("Assets/Textures/red.png"),
		&m_player,
		MonsterType::Stirge));
	m_enemies.push_back(new Enemy(m_enemyMesh,
		m_unlitTexturedShader,
		m_textureManager->GetTexture("Assets/Textures/red.png"),
		&m_player,
		MonsterType::Kobold));
	m_enemies.push_back(new Enemy(m_enemyMesh,
		m_unlitTexturedShader,
		m_textureManager->GetTexture("Assets/Textures/red.png"),
		&m_player,
		MonsterType::Skeleton));
	m_enemies.push_back(new Enemy(m_enemyMesh,
		m_unlitTexturedShader,
		m_textureManager->GetTexture("Assets/Textures/red.png"),
		&m_player,
		MonsterType::Ogre));
	m_enemies.push_back(new Enemy(m_enemyMesh,
		m_unlitTexturedShader,
		m_textureManager->GetTexture("Assets/Textures/red.png"),
		&m_player,
		MonsterType::Dragon));
	for (int i = 0; i < 5; i++) {

		m_gameObjects.push_back(m_enemies[i]);

	}

	int count = 0;
	for (int j = 0; j < 4; j++) { //Sets the location of the outside walls

		for (int i = 0; i < 11; i++) {

			m_walls.push_back(new GameObject(m_wallMesh, m_unlitTexturedShader, m_textureManager->GetTexture("Assets/Textures/gray.png")));
			m_gameObjects.push_back(m_walls[count]);

			switch (j) {
			case 0:
				m_walls[count]->SetPosition(Vector3(i - 1, 0, 10));
				break;
			case 1:
				m_walls[count]->SetPosition(Vector3(10, 0, i - 1));
				break;
			case 2:
				m_walls[count]->SetPosition(Vector3(i - 1, 0, -1));
				break;
			case 3:
				m_walls[count]->SetPosition(Vector3(-1, 0, i - 1));
				break;
			}
			m_walls[count]->SetBounds();
			count++;

		}

	}

	m_board.resize(10, std::vector<GameObject*>(10, 0));

	MakeBoard(); //Makes the board after all the tiles are done

	m_gameObjects.push_back(m_player = new Player(m_input)); //Makes a player

	m_player->SetTeleport(&m_teleCapsules);

	m_clips.resize(6, std::vector<Bullet*>(0, 0));
	for (int i = 0; i < 6; i++) {

		for (int j = 0; j < 10; j++) {

			m_clips[i].push_back(new Bullet(m_bulletMesh, m_unlitTexturedShader, m_textureManager->GetTexture("Assets/Textures/bullet.png")));
			m_gameObjects.push_back(m_clips[i][j]);

		}

	}
	m_player->SetClip(&m_clips[0]);
	for (int i = 0; i < m_enemies.size(); i++) {

		m_enemies[i]->SetClip(&m_clips[i + 1]);

	}

}

void Game::MakeBoard() {

	bool made = false;

	for (int i = 0; i < 10; i++) {

		for (int j = 0; j < 10; j++) {

			m_tiles[i + j * 10]->SetPosition(Vector3(i, 0, j));
			m_board[i][j] = m_tiles[i + j * 10];

		}

	}
	std::vector<GameObject*>* tiles = &m_tiles;

	while (!made) {

		int count = 0;
		made = true;
		std::random_shuffle((*tiles).begin(), (*tiles).end());
		for (int i = 0; i < m_teleCapsules.size(); i++) {

			if ((*tiles)[i]->GetPosition().x < 1 && (*tiles)[i]->GetPosition().z < 1) {

				made = false;
				break;

			}
			m_teleCapsules[i]->SetPosition((*tiles)[i]->GetPosition());

		}
		if (made) {

			count += m_teleCapsules.size();
			for (int i = 0; i < m_healthCapsules.size(); i++) {

				if ((*tiles)[i + count]->GetPosition().x < 1 && (*tiles)[i + count]->GetPosition().z < 1) {

					made = false;
					break;

				}
				m_healthCapsules[i]->SetPosition((*tiles)[i + count]->GetPosition());

			}

		}
		if (made) {

			count += m_healthCapsules.size();
			for (int i = 0; i < m_enemies.size(); i++) {

				if ((*tiles)[i + count]->GetPosition().x < 4 && (*tiles)[i + count]->GetPosition().z < 4) {

					made = false;
					break;

				}
				m_enemies[i]->SetPosition((*tiles)[i + count]->GetPosition());

			}

		}

	}

}

void Game::Update(float timestep) {

	m_input->BeginUpdate();

	if (!m_gameOver) { //Only update the world if the game is not over

		for (int i = 0; i < m_gameObjects.size(); i++) m_gameObjects[i]->Update(timestep);

		if (m_player->checkGameOver()) {

			m_gameOver = true;
			ShowCursor(true); //Show the cursor to select to continue or not

		}

		m_collisionManager->CheckCollisions();

		m_currentCam->Update(timestep);

		//SetCursorPos(960, 540); //Keep the cursor in the middle of the screen

	}
	else { //If the game is over, only update the buttons

		for (int i = 0; i < m_buttons.size(); i++) {

			m_buttons[i]->Update();

		}

	}

	m_input->EndUpdate();

}

void Game::Render() {

	m_renderer->BeginScene(0.1f, 0.1f, 0.1f, 1.0f);

	for (unsigned int i = 0; i < m_gameObjects.size(); i++) m_gameObjects[i]->Render(m_renderer, m_currentCam);

	if (!m_gameOver) DrawUI(); //Draw the regular UI if the game is not over
	else DrawGameOverUI(); //Otherwise draw the game over UI

	m_renderer->EndScene();

}

void Game::Restart() {

	MakeBoard(); //Randomise the board again, and resetting it

	m_player->Reset(); //Reset the player with the new board

	for (int i = 0; i < m_clips.size(); i++) {

		for (int j = 0; j < m_clips[i].size(); j++) {

			m_clips[i][j]->Disable();

		}

	}

	for (int i = 0; i < m_enemies.size(); i++) {

		m_enemies[i]->Reset();

	}

	m_gameOver = false;

	ShowCursor(false); //Hide the cursor again

}

void Game::DrawUI() {

	m_renderer->SetCurrentShader(NULL);

	CommonStates states(m_renderer->GetDevice());
	m_spriteBatch->Begin(SpriteSortMode_Deferred, states.NonPremultiplied());

	RefreshUI();

	m_spriteFont->DrawString(m_spriteBatch, m_score.c_str(), Vector2(20, 150), Color(1, 1, 1), 0, Vector2(1, 1));

	m_spriteBatch->Draw(m_healthSprite->GetShaderResourceView(), Vector2(440, 20), m_healthSize, Color(1.0f, 1.0f, 1.0f));

	m_spriteBatch->End();

}

void Game::DrawGameOverUI() {

	m_renderer->SetCurrentShader(NULL);

	CommonStates states(m_renderer->GetDevice());
	m_spriteBatch->Begin(SpriteSortMode_Deferred, states.NonPremultiplied());

	m_spriteBatch->Draw(m_grayBack->GetShaderResourceView(), Vector2(0, 0), m_screenSize, Color(0.1f, 0.1f, 0.1f));

	m_spriteFont->DrawString(m_spriteBatch, m_gameOverTxt.c_str(), Vector2(550, 150), Color(1, 1, 1), 0, Vector2(1, 1));
	m_spriteFont->DrawString(m_spriteBatch, m_continue.c_str(), Vector2(575, 350), Color(1, 1, 1), 0, Vector2(1, 1));

	m_buttonYes->Render();
	m_buttonNo->Render();

	m_spriteBatch->End();

}

void Game::Shutdown() {

	if (m_currentCam){

		delete m_currentCam;
		m_currentCam = NULL;

	}
	if (m_shader){

		m_shader->Release();
		delete m_shader;
		m_shader = NULL;

	}
	if (m_tileMesh) m_tileMesh = NULL;

	if (m_playerMesh) m_playerMesh = NULL;

	if (m_player) {

		m_player->Release();
		m_player = NULL;

	}
	if (m_tiles[0]) {

		for (int i = 0; i < m_tiles.size(); i++) {

			m_tiles[i] = NULL;

		}

	}
	if (m_board[0][0]) {

		for (int i = 0; i < m_board.size(); i++) {

			for (int j = 0; j < m_board[i].size(); j++) {

				m_board[i][j] = NULL;

			}

		}
		
	}

}