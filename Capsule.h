#pragma once
#include "GameObject.h"
#include "TileTypes.h"

class Capsule : public GameObject {
protected:
	bool m_enabled = true;

	Type m_type;
	Vector3 m_offset = Vector3(0, 0.125f, 0);
	Vector3 m_rotationOffset = Vector3(0, -0.25f, 0);
	float m_rotationSpeed = 10.0f;
	float m_teleScale = 0.125f;
	float m_rotationMax = 360.0f;

	CBoundingSphere m_boundingSphere;

	float m_floatBuffer = 0.1f;
	float m_floatShift = 0.25f;
	float m_floatSin = 0.0f;

	bool m_floatingUp = true;

	void SetStartRotation() { m_rotationX = ToRadians(90); }
	void SetStartScale() { m_scale = 0.5f; }

public:
	Capsule(Mesh* mesh, Shader* shader, Texture* texture, Type type);
	Type GetType() { return m_type; }

	virtual void SetPosition(Vector3 position);
	virtual void Update(float timestep);
	virtual void Render(Direct3D* renderer, Camera* cam);
	void Disable() { m_enabled = false; }
	void Reset() { m_enabled = true; }
	bool GetEnabled() { return m_enabled; }

};