#ifndef TILETYPES_H
#define TILETYPES_H

#include "Texture.h"

enum class Type { //Types of tiles
	Heal,
	Teleport,
	Disabled,
	Monster,
	Clean
};

enum class MonsterType { //Types of the Monster tile
	Stirge,
	Kobold,
	Skeleton,
	Ogre,
	Dragon
};

#endif
