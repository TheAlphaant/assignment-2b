#pragma once

#include "Camera.h"
#include "GameObject.h"
#include "InputController.h"
#include "MathsHelper.h"
#include "Player.h"

class CameraMoving : public Camera {

private:
	Player* m_player = NULL;
	InputController* m_input;

	float m_heading;
	float m_pitch;
	float m_lookSpeed = 2.0f;
	float m_followStrength = 1;

public:
	CameraMoving(Player* object);
	CameraMoving(Player* object, InputController* input);
	CameraMoving(Player* object, float strength);

	void Update(float timestep);
	void setTarget(Player* target) { m_player = target; }
	bool mapLook;
	float GetHeading() { return m_heading; }

};