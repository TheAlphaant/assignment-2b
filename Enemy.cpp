#include "Enemy.h"

Enemy::Enemy(Mesh* mesh, Shader* shader, Texture* texture, Player* *player, MonsterType type) :
	GameObject(mesh, shader, texture){

	m_player = player;
	m_type = type;
	m_scale = m_startScale;
	Initialize();

}

void Enemy::Initialize() {

	m_health = MathsHelper::RandomRange(m_healthBottom, m_healthTop);
	if (m_health > m_health1) m_moveSpeed = m_speed1;
	else if (m_health > m_health2) m_moveSpeed = m_speed2;
	else if (m_health > m_health3) m_moveSpeed = m_speed3;
	else if (m_health > m_health4) m_moveSpeed = m_speed4;
	else m_moveSpeed = m_speed5;

	if(m_type == MonsterType::Dragon){

		m_playerEnemyBuffer.SetRadius(m_playerEnemyBufferSize);
		m_target = RandomSpot();

	}
	m_enabled = true;

}

void Enemy::Update(float timestep) {

	if (m_enabled) {

		m_currentTimestep = timestep;

		Vector3 lookAt = (*m_player)->GetPosition() - m_position;
		m_rotationY = atan2(lookAt.x, lookAt.z);

		switch (m_type) {
		case MonsterType::Stirge:
			m_target = (*m_player)->GetPosition();
			break;
		case MonsterType::Kobold:
			m_target = (*m_player)->GetPosition();
			break;
		case MonsterType::Skeleton:
			if (Vector3::Distance(m_target, m_position) < m_moveBuffer) {
				m_target = RandomSpot();
			}
			break;
		case MonsterType::Ogre: {
			Matrix playerHeading = Matrix::CreateRotationY((*m_player)->GetHeading());
			Vector3	localPlayerForward = Vector3::TransformNormal(worldForward, playerHeading);
			m_target = (*m_player)->GetPosition() + localPlayerForward;
			break;
		}
		case MonsterType::Dragon: {
			m_playerEnemyBuffer.SetCenter((*m_player)->GetPosition());
			if (CheckCollision(m_playerEnemyBuffer, m_boundingBox) && Vector3::Distance(m_target, m_position) < m_moveBuffer) {
				m_target = RandomSpot();
			}
			break;
		}

		}

		Vector3 distance = m_target - m_position;
		distance.Normalize();

		if (m_type == MonsterType::Kobold) m_position -= distance * timestep * m_moveSpeed;
		else m_position += distance * timestep * m_moveSpeed;
		m_position.y = m_startCount;

		m_boundingBox.SetMin(m_position + m_mesh->GetMin() * m_scale);
		m_boundingBox.SetMax(m_position + m_mesh->GetMax() * m_scale);

		m_shootBuffer += timestep;
		if (m_shootBuffer - m_lastShootBuffer > m_shootTime) {

			Shoot();
			m_lastShootBuffer = m_shootBuffer;
			m_shootTime = MathsHelper::RandomRange(m_shootTimeTop, m_shootTimeBottom);

		}

	}

}

void Enemy::Render(Direct3D* renderer, Camera* cam) {

	if (m_enabled) GameObject::Render(renderer, cam);

}

void Enemy::WallCollision(GameObject* *wall) {

	if ((*wall)->GetPosition().x < m_startCount) {

		m_position += worldRight * m_moveSpeed * m_currentTimestep;

	}
	else if ((*wall)->GetPosition().x > m_worldTop) {

		m_position -= worldRight * m_moveSpeed * m_currentTimestep;

	}
	else if ((*wall)->GetPosition().z < m_startCount) {

		m_position += worldForward * m_moveSpeed * m_currentTimestep;

	}
	else if ((*wall)->GetPosition().z > m_worldTop) {

		m_position -= worldForward * m_moveSpeed * m_currentTimestep;

	}

}

void Enemy::BulletCollision(Bullet* *bullet) {

	(*bullet)->Disable();

	m_health -= (*bullet)->GetDamage();

	if (CheckDeath()) {

		(*m_player)->GetKill();

	}

}

bool Enemy::CheckDeath() {

	if (m_health <= m_startCount) {

		m_enabled = false;
		return true;

	}
	return false;

}

void Enemy::Shoot() {

	(*m_bullets)[m_shootCount]->Enable();
	(*m_bullets)[m_shootCount]->SetPosition(m_position + Vector3::TransformNormal(m_bulletOffset, Matrix::CreateRotationY(m_rotationY)));
	(*m_bullets)[m_shootCount]->SetRotY(m_rotationY);
	(*m_bullets)[m_shootCount]->SetDamage(MathsHelper::RandomRange(m_damageDown, m_damageUp) * m_health);

	if (m_shootCount >= (*m_bullets).size() - 1) m_shootCount -= (*m_bullets).size() - 1;
	else m_shootCount++;

}