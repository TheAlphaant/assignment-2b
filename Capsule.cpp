#include "Capsule.h"

Capsule::Capsule(Mesh* mesh, Shader* shader, Texture* texture, Type type) :
	GameObject(mesh, shader, texture) {

	m_type = type;
	SetStartScale();
	SetStartRotation();
	m_boundingSphere.SetRadius(m_scale * m_scale * m_scale);

}

void Capsule::Update(float timestep) {

	if (m_enabled) {

		if (m_type == Type::Teleport) {

			m_rotationY += timestep * m_rotationSpeed;

		}
		else if (m_type == Type::Heal) {

			m_rotationY += timestep;
			m_floatSin += timestep;
			m_position.y = m_floatShift + sin(m_floatSin) * m_floatBuffer;

		}
		if (ToDegrees(m_rotationY) > m_rotationMax) m_rotationY -= ToRadians(m_rotationMax);
		if (ToDegrees(m_floatSin) > m_rotationMax) m_floatSin -= ToRadians(m_rotationMax);
		SetBounds();

		m_boundingBox.SetMin(m_position + m_mesh->GetMin());
		m_boundingBox.SetMax(m_position + m_mesh->GetMax());

	}

}

void Capsule::SetPosition(Vector3 position) {

	m_position = position;
	m_position += m_offset;

}

void Capsule::Render(Direct3D* renderer, Camera* cam) {

	if (m_enabled) {

		if (m_type == Type::Heal) m_teleScale = m_scale;

		m_world = Matrix::CreateScale(Vector3(m_scale, m_scale, m_teleScale)) *
			Matrix::CreateTranslation(m_rotationOffset) *
			Matrix::CreateRotationX(m_rotationX) * Matrix::CreateRotationY(m_rotationY) *
			Matrix::CreateTranslation(m_position);
		m_mesh->Render(renderer, m_shader, m_world, cam, m_texture);

	}

}