#include "CameraMoving.h"

CameraMoving::CameraMoving(Player* object) {

	m_player = object;
	SetPosition(m_player->GetPosition());

}

CameraMoving::CameraMoving(Player* object, InputController* input) {

	m_player = object;
	m_input = input;
	SetPosition(m_player->GetPosition());

}

CameraMoving::CameraMoving(Player* object, float strength) {

	m_player = object;
	m_followStrength = strength;
	SetPosition(m_player->GetPosition());

}

void CameraMoving::Update(float timestep) {

	if (m_player) {

		if (ToDegrees(GetHeading()) >= 360) m_heading -= m_heading;
		else if (GetHeading() < 0) m_heading += ToRadians(360);

		m_heading += m_input->GetMouseDeltaX() * m_lookSpeed * timestep;
		m_pitch += m_input->GetMouseDeltaY() * m_lookSpeed * timestep;
		m_player->SetHeading(m_heading);

		m_pitch = MathsHelper::Clamp(m_pitch, ToRadians(-80.0f), ToRadians(80.0f));

		Matrix heading = Matrix::CreateRotationY(GetHeading());
		Matrix pitch = Matrix::CreateRotationX(m_pitch);

		Matrix lookAtRotation = pitch * heading;
		Vector3 lookAt = Vector3::TransformNormal(Vector3(0, 0, 1), lookAtRotation);

		lookAt += m_player->GetPosition();

		SetLookAt(lookAt);
		SetPosition(m_player->GetPosition());

	}

	Camera::Update(timestep);

}