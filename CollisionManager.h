#ifndef COLLISION_MANAGER_H
#define COLLISION_MANAGER_H

#include <vector>
#include "Collisions.h"
#include "Player.h"
#include "Capsule.h"
#include "Enemy.h"
#include "Bullet.h"

#define MAX_ALLOWED_COLLISIONS 2048

class CollisionManager
{
private:
	Player* m_player;
	std::vector<Enemy*>* m_enemies;
	std::vector<GameObject*>* m_walls;
	std::vector<GameObject*>* m_tiles;
	std::vector<Capsule*>* m_capsules;
	std::vector<std::vector<Bullet*>>* m_bullets;

	GameObject* m_currentCollisions[MAX_ALLOWED_COLLISIONS];

	// We need to know what objects were colliding last frame so we can determine if a collision has just begun or ended
	GameObject* m_previousCollisions[MAX_ALLOWED_COLLISIONS];

	int m_nextCurrentCollisionSlot;

	// Check if we already know about two objects colliding
	bool ArrayContainsCollision(GameObject* arrayToSearch[], GameObject* first, GameObject* second);

	// Register that a collision has occurred
	void AddCollision(GameObject* first, GameObject* second);

	// Collision check helpers
	void PlayerToCapsule();
	void PlayerToWall();
	void PlayerToBullet();
	void EnemyToWall();
	void EnemyToBullet();
	void PlayerToEnemy();
	void BulletToWall();

	int m_startCount = 0;

public:
	CollisionManager(Player* player, std::vector<Enemy*>* enemies, std::vector<GameObject*>* walls, std::vector<GameObject*>* tiles, std::vector<Capsule*>* capsules, std::vector<std::vector<Bullet*>>* bullets);
	void CheckCollisions();

};

#endif