#pragma once

#include "Direct3D.h"
#include "Mesh.h"
#include "Texture.h"
#include "Camera.h"
#include "Collisions.h"
#include "MathsHelper.h"
#include "TileTypes.h"

#include <vector>

class GameObject {
protected:
	Vector3 m_position;
	Matrix m_world = Matrix::Identity;
	Mesh* m_mesh;
	Shader* m_shader;
	Texture* m_texture;
	CBoundingBox m_boundingBox;

	Vector3 worldForward = Vector3(0, 0, 1);
	Vector3 worldRight = Vector3(1, 0, 0);

	float m_scale = 1.0f;
	float m_rotationX = 0.0f;
	float m_rotationY = 0.0f;
	float m_rotationZ = 0.0f;

public:
	GameObject();
	GameObject(Mesh* mesh, Shader* shader, Texture* texture);

	virtual void Render(Direct3D* renderer, Camera* cam);
	virtual void SetPosition(Vector3 position) { m_position = position; }
	void SetMesh(Mesh* mesh) { m_mesh = mesh; }
	void SetShader(Shader* shader) { m_shader = shader; }
	void SetTexture(Texture* texture) { m_texture = texture; }
	void SetScale(float scale) { m_scale = scale; }
	virtual void Update(float timestep) { ; }
	void SetRotX(float rotX) { m_rotationX = rotX; }
	void SetRotY(float rotY) { m_rotationY = rotY; }
	void SetRotZ(float rotZ) { m_rotationZ = rotZ; }

	Vector3 GetPosition() { return m_position; }
	virtual void SetBounds();

	CBoundingBox GetBounds() { return m_boundingBox; }

};