#include "GameObject.h"

GameObject::GameObject() {
}

GameObject::GameObject(Mesh* mesh, Shader* shader, Texture* texture) {

	m_mesh = mesh;
	m_shader = shader;
	m_texture = texture;
	SetBounds();

}

void GameObject::Render(Direct3D* renderer, Camera* cam) {

	if (m_mesh) {

		m_world = Matrix::CreateScale(Vector3(m_scale, m_scale, m_scale)) * Matrix::CreateFromYawPitchRoll(m_rotationY, m_rotationX, m_rotationZ) * Matrix::CreateTranslation(m_position);
		m_mesh->Render(renderer, m_shader, m_world, cam, m_texture);

	}

}

void GameObject::SetBounds() {

	m_boundingBox = CBoundingBox(m_position + m_mesh->GetMin(), m_position + m_mesh->GetMax());

}