#ifndef PLAYER_H
#define PLAYER_H

#include "InputController.h"
#include "GameObject.h"
#include "Bullet.h"
#include "Capsule.h"

#include <math.h>
#include <vector>

class Player : public GameObject {

private:
	InputController* m_input;
	Vector3 m_lastPosition;

	Vector3 m_startPosition = Vector3(0, 0.5f, 0);

	CBoundingSphere m_boundingSphere;
	float m_playerRadius = 0.125f;

	std::vector<Capsule*>* m_teleportList;

	std::vector<Bullet*>* m_bullets;
	float m_lastShootBuffer;
	float m_shootBuffer;
	float m_shootTime = 0.8f;

	int m_shootCount = 0;
	Vector3 m_bulletOffset = Vector3(0.0f, -0.1f, 0.0f);

	int m_teleportCount = 0;
	int m_startCount = 0;
	int m_killCount = 0;
	int m_maxKillCount = 5;

	float m_moveSpeed = 0.001f;
	float m_heading;

	bool moving = 0;

	float m_maxHealth = 200.0f;
	float m_health = 200.0f;
	float m_healthIncrement = 50.0f;
	float m_damageUp = 30.0f;
	float m_damageDown = 20.0f;

	void teleport(Capsule* capsule);
	void heal();
	void setStart();
	bool checkPlayer();

	float wallCollisionBuffer = 10.0f;
	float m_currentTimestep;
	float m_diagBuffer = 0.7f;;

	void Initialize();

public:
	Player::Player(InputController* input);

	virtual void Update(float timestep);
	void Release();
	void Reset();
	bool checkGameOver();

	void GetKill() { m_killCount++; }
	int GetKillCount() { return m_killCount; }
	float GetHealth() { return m_health; }

	void SetHeading(float heading) { m_heading = heading; }
	float GetHeading() { return m_heading; }

	void WallCollision(GameObject* *wall);
	void CapsuleCollision(Capsule* *capsule);
	void EnemyCollision() { m_health -= m_maxHealth; }
	void BulletCollision(Bullet* *bullet);
	void SetTeleport(std::vector<Capsule*>* capsule) { m_teleportList = capsule; }

	void Shoot();

	void SetClip(std::vector<Bullet*>* clip) { m_bullets = clip; }

	CBoundingSphere GetBounds() { return m_boundingSphere; }

};

#endif